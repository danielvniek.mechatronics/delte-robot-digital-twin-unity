﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class diskGenerator : MonoBehaviour
{
    public GameObject diskPrefab;
    private GameObject newDisk;
    public static List<GameObject> allTheDisks = new List<GameObject>();
    float t = 0;
    float tTemp = 0;
    int ColGen = 0;
    public static bool DiskReady = false;
    private float z = 0f;
    private float waitingTime = 0f;
    private static float mintime = (1 / beltGenerator.beltSpeed);
    private static float maxtime = mintime + 3;
    Vector3 spawnpos;
    public static bool DiskColor=false;//default red
    public static int diskReadyIndex = 0;
    public static float xAtReadyPoint = 0;
    public static float zAtReadyPoint = 0;
    public static float yAtReadyPoint = 0;
    private float diskThickness = 0.02f;
    
    // Start is called before the first frame update
    void Start()
    {
        waitingTime = Random.Range(mintime, maxtime);
        z = Random.Range(-0.35f, 0.35f);
        spawnpos = new Vector3(-5f, 0f, z);
    }

    // Update is called once per frame
    void Update()
    {
        t = t + Time.deltaTime;
       


       
        if ((t >= waitingTime)&&(DiskReady==false)&&(ADSConnection.BeltFeed==true))
        {
            ColGen = Random.Range(0, 10);
            
            newDisk = Instantiate(diskPrefab, spawnpos, Quaternion.identity);
            if (ColGen >= 5)
            {
                newDisk.GetComponent<MeshRenderer>().material.color = Color.red;
            }
            else
            {
                newDisk.GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            allTheDisks.Add(newDisk);
            t = 0;
            waitingTime = Random.Range(mintime, maxtime);
            z = Random.Range(-0.35f, 0.35f);
            spawnpos = new Vector3(-5.5f, 0f, z);
        }
        if (DiskReady == false)
        {
            for (int i = 0; i < allTheDisks.Count; i++)
            {

                if ((allTheDisks[i].transform.position.x >= -0.25)&&(allTheDisks[i].transform.position.x <-0f)&&(ADSConnection.BeltFeed==true))//condition to be added
                {
                    diskReadyIndex = i;
                    xAtReadyPoint = allTheDisks[i].transform.position.x;
                    zAtReadyPoint = allTheDisks[i].transform.position.z;
                    
                    yAtReadyPoint = allTheDisks[i].transform.position.y;
                    Debug.Log(yAtReadyPoint);
                    if (allTheDisks[i].GetComponent<MeshRenderer>().material.color == Color.blue)
                    {
                        DiskColor = true;

                    }
                    if (allTheDisks[i].GetComponent<MeshRenderer>().material.color == Color.red)
                    {
                        DiskColor = false;

                    }
                   

                    Debug.Log(DiskColor);
                    DiskReady = true;
                }

            }
        }
        else
        {
           
            if (allTheDisks[diskReadyIndex].transform.position.y > yAtReadyPoint+diskThickness) {
                DiskReady = false;
            }
        }


    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBeltGenerator : MonoBehaviour
{
    public GameObject beltPrefab;
    private GameObject newBelt;
    public static List<GameObject> allTheBelts1 = new List<GameObject>();
    public static List<GameObject> allTheBelts2 = new List<GameObject>();
    public static float beltSpeed = 0.4f;
    static float beltSize = 0.04f;
    private static float ConveyorLength = 5.4f;
    float beltLimit = 5.5f  - beltSize / 2;
    float SpawnX = 2.8f - (ConveyorLength / 2) + (beltSize / 2);
    float SpawnZ = 0.2f;
    



    int j = 0;

    private static Vector3 spawnpos;
    // Start is called before the first frame update
    void Start()
    {
        spawnpos = new Vector3(SpawnX, -1.094f, SpawnZ);
        for (int i = 0; i < (ConveyorLength / beltSize); i++)
        {
            newBelt = Instantiate(beltPrefab, spawnpos, Quaternion.identity);
            allTheBelts1.Add(newBelt);
            spawnpos.x = spawnpos.x + beltSize;
        }
        spawnpos = new Vector3(SpawnX, -1.094f, -1*SpawnZ);
        for (int i = 0; i < (ConveyorLength / beltSize); i++)
        {
            newBelt = Instantiate(beltPrefab, spawnpos, Quaternion.identity);
            allTheBelts2.Add(newBelt);
            spawnpos.x = spawnpos.x + beltSize;
        }


    }

    // Update is called once per frame
   void Update()
    {
        
            beltSpeed = 0.4f;


            for (int i = 0; i < allTheBelts1.Count; i++)
            {
                allTheBelts1[i].GetComponent<Rigidbody>().velocity = new Vector3(beltSpeed, 0, 0);
                allTheBelts2[i].GetComponent<Rigidbody>().velocity = new Vector3(beltSpeed, 0, 0);
            j = i + 1;
                if (j >= allTheBelts1.Count)
                {
                    j = 0;
                }
                if ((allTheBelts1[i].transform.position.x >= beltLimit))
                {
                    spawnpos.x = allTheBelts1[j].transform.position.x - beltSize;
                    spawnpos.z = SpawnZ;
                    allTheBelts1[i].transform.position = spawnpos;
                      spawnpos.z = -1*SpawnZ;
                     allTheBelts2[i].transform.position = spawnpos;

            }
            }

       

    }
}

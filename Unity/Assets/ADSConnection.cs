﻿using UnityEngine;
using UnityEngine.UI;
using TwinCAT.Ads;


public class ADSConnection : MonoBehaviour					//No further variables need to be created
{
    //Create a new instance of class TcAdsClient
    TcAdsClient tcClient = new TcAdsClient();

    //Assignment of public variables via the Unity Inspector
    //Creation of GameObjects for controlling the arms and reading the position data of the TCP
    public GameObject[] arms=new GameObject[3];
    public GameObject tcp;

    //text variables for the UI 
    public Text tcp_x_pos;
    public Text tcp_y_pos;
    public Text tcp_z_pos;
    //variables for the position of the TCP
    float pos_x;
    float pos_y;
    float pos_z;
    //Variables for controlling the upper arm angles
    float x_last=0;
    float y_last=0;
    float z_last=0;
    float x_act=0;
    float y_act=0;
    float z_act=0;

    static float offset = 9.636f;       		//Offset of arm angle from Solidworks model to reality
    static float scalíngfactor = 1000;      //Scaling factor as TwinCAT works in mm and Unity in m
    private float speed = -1f;
    private int hDiskReady;
    private bool notified = false;
    public static bool BeltFeed = true;
    private int hBeltFeed;
    private int hDiskColor;
    bool test = true;
    float XposScaled = -250f;
    float YposScaled = -950f;
    float ZposScaled = 30f;
    private int hXPos;
    private int hYPos;
    private int hZPos;
    private int hSuction;
    public static bool suction=false;
    public float TCPtoDisk = 0.1f;
    void Start()
    {
       
        //Correction of the angular offset of the arms; check in Unity which axis actually corresponds to the usual x-, y-, z-axis. 
        arms[0].transform.Rotate(new Vector3( offset, 0, 0));       //For which axis (x, y or z) must the offset be used?
        arms[1].transform.Rotate(new Vector3( offset, 0, 0));
        arms[2].transform.Rotate(new Vector3( offset, 0, 0));
       // arms[2].transform.Rotate(new Vector3(-70f, 0, 0));
      //  arms[1].transform.Rotate(new Vector3(-50f, 0, 0));
        //Establishing the connection to TwinCAT
        tcClient.Connect(851);              //What else do I need to specify to connect to another computer in the same network?
        hDiskReady = tcClient.CreateVariableHandle("MAIN.DiskReady");
        hDiskColor = tcClient.CreateVariableHandle("MAIN.DiskCol");
        hBeltFeed = tcClient.CreateVariableHandle("MAIN.BeltFeed");
        hSuction = tcClient.CreateVariableHandle("MAIN.SuctionEnabled");
        hXPos = tcClient.CreateVariableHandle("MAIN.DiskPosX");
        hYPos = tcClient.CreateVariableHandle("MAIN.DiskPosY");
        hZPos = tcClient.CreateVariableHandle("MAIN.DiskPosZ");
        //Reading the angles from TwinCAT 
        //https://infosys.beckhoff.com/english.php?content=../content/1033/tcsample_net/html/twincat.ads.sample12.htm&id=
        x_last = (float)tcClient.ReadAny(0x4020, 0x0, typeof(float));
        y_last = (float)tcClient.ReadAny(0x4020, 0x10, typeof(float));
        z_last = (float)tcClient.ReadAny(0x4020, 0x20, typeof(float));
        
        //Rotation of the upper arms by the angle change to the previous state
        arms[0].transform.Rotate(new Vector3(-(x_last), 0f, 0f));
        arms[1].transform.Rotate(new Vector3(-y_last, 0f, 0f));
        arms[2].transform.Rotate(new Vector3(-z_last, 0f, 0f));
    }


      void Update()   //Update function suitable here?
      {
        tcClient.WriteAny(hDiskReady, diskGenerator.DiskReady);
        if (diskGenerator.DiskReady == true)
        {
            tcClient.WriteAny(hDiskColor,diskGenerator.DiskColor);
            XposScaled = scalíngfactor * diskGenerator.xAtReadyPoint;
            YposScaled = scalíngfactor * (diskGenerator.yAtReadyPoint+TCPtoDisk);
            ZposScaled = scalíngfactor * diskGenerator.zAtReadyPoint;
            tcClient.WriteAny(hXPos, XposScaled);
            tcClient.WriteAny(hYPos, YposScaled);
            tcClient.WriteAny(hZPos, ZposScaled);
        }
            
        suction = (bool)tcClient.ReadAny(hSuction, typeof(bool));
        BeltFeed = (bool)tcClient.ReadAny(hBeltFeed, typeof(bool));
          //Read the axis angle (3) from TwinCAT; command see start method according to x_last, y_last, z_last
          x_act = (float)tcClient.ReadAny(0x4020, 0x0, typeof(float));
          y_act = (float)tcClient.ReadAny(0x4020, 0x10, typeof(float));
          z_act = (float)tcClient.ReadAny(0x4020, 0x20, typeof(float));
        
          
          //Control the angles in Unity over the difference from the old to the current value.
          //The value change of the angles is always transferred (current value - old value; take all three axes into account). 
          arms[0].transform.Rotate(new Vector3( (x_last - x_act),0f, 0f ));      //Note the sign of the axes
          arms[1].transform.Rotate(new Vector3((y_last - y_act), 0f, 0f));
          arms[2].transform.Rotate(new Vector3((z_last - z_act), 0f, 0f));
        //arms[0].transform.Rotate(new Vector3(speed, 0f, 0f));      //Note the sign of the axes
      //  arms[1].transform.Rotate(new Vector3(speed, 0f, 0f));
      //  arms[2].transform.Rotate(new Vector3(speed, 0f, 0f));
        //Description of the current angular state as old state for the next call of the update function 
        x_last = x_act;
          y_last = y_act;
          z_last = z_act;



          //Reading the position data of the tool carrier and conversion to mm
          pos_x = Mathf.Round(tcp.transform.position.x*scalíngfactor);			//here the scaling factor must be taken into account
          pos_y = Mathf.Round(tcp.transform.position.y*scalíngfactor);
          pos_z = Mathf.Round(tcp.transform.position.z*scalíngfactor);

          settext();		//has to be written

      }
   /* private void Update()
    {
        arms[0].transform.Rotate(new Vector3(speed, 0f, 0f));      //Note the sign of the axes
        arms[1].transform.Rotate(new Vector3(speed, 0f, 0f));
        arms[2].transform.Rotate(new Vector3(speed, 0f, 0f));
        Debug.Log(arms[0].transform.rotation.x);
        if (arms[0].transform.rotation.x == -1f)
        {
            Debug.Log("change");
            speed = -1 * speed;
        }
        if (arms[0].transform.rotation.x == -1f)
        {
            speed = -1 *speed;
        }
    /*/

    //Disconnection of the connection to TwinCAT at the end of the program
    void OnApplicationQuit()
    {
        tcClient.Disconnect();
    }


    //Output of the tool carrier position to the UI (user interface); string output
    //https://docs.unity3d.com/ScriptReference/Object.ToString.html
    void settext() {
        //Debug.Log("in");
        tcp_x_pos.text = "Gripper X-position: " + pos_x.ToString();
        tcp_y_pos.text = "Gripper Y-position: " + pos_y.ToString(); ;
        tcp_z_pos.text = "Gripper Z-position: " + pos_z.ToString();
    }

}
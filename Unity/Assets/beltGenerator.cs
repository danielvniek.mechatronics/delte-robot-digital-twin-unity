﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beltGenerator : MonoBehaviour
{
    public GameObject beltPrefab;
    private GameObject newBelt;
    public static List<GameObject> allTheBelts = new List<GameObject>();
    public static float beltSpeed = 0.4f;
    static float beltSize = 0.04f;
    private static float ConveyorLength = 5.4f;
    float beltLimit = -0.1f + beltSize/2;
    float SpawnX = -2.8f - (ConveyorLength / 2) + (beltSize / 2);
  
  
   
    int j = 0;
 
    private static Vector3 spawnpos;
    // Start is called before the first frame update
    void Start()
    {
        spawnpos = new Vector3(SpawnX, -1.094f, 0f);
        for (int i=0; i < (ConveyorLength / beltSize); i++)
        {
            newBelt = Instantiate(beltPrefab, spawnpos, Quaternion.identity);
            allTheBelts.Add(newBelt);
            spawnpos.x = spawnpos.x + beltSize;
        }
       
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ADSConnection.BeltFeed == true)
        {
            beltSpeed = 0.4f;

             
           
            for (int i = 0; i < allTheBelts.Count; i++)
            {
                allTheBelts[i].GetComponent<Rigidbody>().velocity = new Vector3(beltSpeed, 0, 0);
                j = i + 1;
                if (j >= allTheBelts.Count)
                {
                    j = 0;
                }
                if ((allTheBelts[i].transform.position.x >= beltLimit))
                {
                    spawnpos.x = allTheBelts[j].transform.position.x - beltSize;
                    allTheBelts[i].transform.position = spawnpos;
                    
                                      
                }
            }
          
        }
        else
        {
            for (int i = 0; i < allTheBelts.Count; i++)
            {
                beltSpeed = 0f;
                allTheBelts[i].GetComponent<Rigidbody>().velocity = new Vector3(beltSpeed, 0, 0);
                
            }
        }
        
    }
}

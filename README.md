# Description #
This robotics project simulates a Delta robot,three belts, a gripper and a colour recognision sensor in a factory used to sort disks by colour. A full explanation of what this simulated system does, how it works and a demonstration can be seen in this video: \
https://www.youtube.com/watch?v=T72VALjWNIY

# Screenshots #
<img src = "/doc/pp.JPG" height = "600"> \
<img src = "/doc/twincat.jpg" height = "600"> \
